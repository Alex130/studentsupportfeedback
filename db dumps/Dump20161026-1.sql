CREATE DATABASE  IF NOT EXISTS `feedback` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `feedback`;
-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: feedback
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `A_administratorID` int(11) NOT NULL AUTO_INCREMENT,
  `A_username` varchar(45) NOT NULL,
  PRIMARY KEY (`A_administratorID`),
  UNIQUE KEY `administratorID_UNIQUE` (`A_administratorID`),
  UNIQUE KEY `A_username_UNIQUE` (`A_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `T_answerID` int(11) NOT NULL AUTO_INCREMENT,
  `T_questionID` int(11) NOT NULL DEFAULT '0',
  `T_answer1` varchar(126) DEFAULT NULL,
  `T_answer2` varchar(126) DEFAULT NULL,
  `T_answer3` varchar(126) DEFAULT NULL,
  `T_answer4` varchar(126) DEFAULT NULL,
  `T_answer5` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`T_answerID`,`T_questionID`),
  KEY `T_questionID_idx` (`T_questionID`),
  CONSTRAINT `T_questionID` FOREIGN KEY (`T_questionID`) REFERENCES `questions` (`Q_questionID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kiosks`
--

DROP TABLE IF EXISTS `kiosks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kiosks` (
  `K_kioskID` int(11) NOT NULL AUTO_INCREMENT,
  `L_kioskName` varchar(45) NOT NULL,
  PRIMARY KEY (`K_kioskID`),
  UNIQUE KEY `L_locationsID_UNIQUE` (`K_kioskID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kiosks`
--

LOCK TABLES `kiosks` WRITE;
/*!40000 ALTER TABLE `kiosks` DISABLE KEYS */;
/*!40000 ALTER TABLE `kiosks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `Q_questionID` int(11) NOT NULL AUTO_INCREMENT,
  `Q_question` varchar(126) NOT NULL,
  `Q_questionCreationDate` date NOT NULL,
  `Q_locationID` int(11) NOT NULL,
  PRIMARY KEY (`Q_questionID`,`Q_locationID`),
  UNIQUE KEY `questionID_UNIQUE` (`Q_questionID`),
  KEY `Q_locationID_idx` (`Q_locationID`),
  CONSTRAINT `Q_locationID` FOREIGN KEY (`Q_locationID`) REFERENCES `locations` (`L_locationID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `results` (
  `R_resultID` int(11) NOT NULL AUTO_INCREMENT,
  `R_resultCreationDateTime` varchar(45) NOT NULL,
  `R_locationID` int(11) NOT NULL,
  `R_questionID` int(11) NOT NULL,
  `R_result` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`R_resultID`),
  UNIQUE KEY `idresults_UNIQUE` (`R_resultID`),
  KEY `R_locationID_idx` (`R_locationID`),
  KEY `R_questionID_idx` (`R_questionID`),
  CONSTRAINT `R_locationID` FOREIGN KEY (`R_locationID`) REFERENCES `locations` (`L_locationID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `R_questionID` FOREIGN KEY (`R_questionID`) REFERENCES `questions` (`Q_questionID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'feedback'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-26 13:28:54
